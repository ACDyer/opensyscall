#pragma once


#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>



ssize_t my_write(int fd, const void *buffer, size_t size);
int my_open(const char* filename, int flags, mode_t mode);
