#include "syscall.hh"
#include <iostream>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#define ONEGB (1000000000)
int main (int argc, char *argv[])
{
	//Clocks
	clock_t start_t, end_t, total_t;
 	//converting cmd line to Buffer int
	int Buffer = strtol(argv[1], NULL, 10);
	char BuffChar[Buffer];
	int myDirectory;
	myDirectory = my_open("Bufftest.txt",O_RDWR|O_CREAT|O_TRUNC,0666);
	double timesToWrite = (ONEGB/Buffer);
	start_t = clock();
	for(int i = 0; i < timesToWrite; i++)
	{
		my_write(myDirectory, BuffChar, Buffer);
	}
	end_t = clock();
	//The total_t is the total time it took to write divided by clocks.
	total_t = (double) (end_t-start_t)/CLOCKS_PER_SEC;
	double writeRate = ONEGB/total_t;
	std::cout << "Write Rate"<< writeRate << std::endl;
	std::cout << "time" << total_t << std::endl;
	close(myDirectory);
 	return (0);
}
